import {FC, useEffect} from "react";
import {useProducts} from "../api/queries/useProducts.ts";
import {usePasswordAuthenticationContext} from "../context/PasswordAuthenticationContext.ts";
import {useWatchdog} from "../api/queries/useWatchdog.ts";
import {Header} from "../components/Header.tsx";
import {Loader} from "../components/Loader.tsx";
import {WatchdogConfiguration} from "../components/WatchdogConfiguration.tsx";
import {Footer} from "../components/Footer.tsx";

export const WatchdogConfigurationScreen: FC = () => {
    const {setPassword} = usePasswordAuthenticationContext();

    const {data: products, isLoading: productsLoading, status: productsQueryStatus} = useProducts();
    const {data: watchdog, isLoading: watchdogLoading} = useWatchdog();

    useEffect(() => {
        if (productsQueryStatus === "error") {
            setPassword(null);
            console.error("Products cannot be loaded. Perhaps an invalid password was provided?");
        }
    }, [productsQueryStatus, setPassword]);

    const loading = productsLoading || watchdogLoading;

    return (
        loading
            ? <Loader/>
            : (
                <div className="flex flex-col items-stretch justify-start min-h-screen">
                    <Header/>
                    <WatchdogConfiguration
                        watchdog={watchdog!}
                        products={products!}
                    />
                    <Footer/>
                </div>
            )
    );
}
