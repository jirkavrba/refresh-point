import {FC, KeyboardEventHandler, useState} from "react";
import {usePasswordAuthenticationContext} from "../context/PasswordAuthenticationContext.ts";

export const LoginScreen: FC = () => {
    const {setPassword: submitPassword} = usePasswordAuthenticationContext();
    const [password, setPassword] = useState("");

    const handleSubmit = () => {
        const trimmed = password.trim();

        if (trimmed.length > 0) {
            submitPassword(trimmed);
        }
    };

    const handleKeyDown: KeyboardEventHandler<HTMLInputElement> = (event) => {
        if (event.key === "Enter") {
            handleSubmit();
        }
    }

    return (
        <main className="flex flex-column items-center justify-center bg-gradient-to-b from-neutral-100 to-neutral-300 w-screen min-h-screen px-4">
            <div className="flex flex-col items-center justify-center gap-4 bg-white p-8 rounded-xl shadow-xl">
                <img src={"/logo.png"} alt={"RefreshPoint"} className="w-16 h-16"/>
                <h1 className="text-4xl font-bold text-neutral-900 mx-8">
                    <span className="text-freshpoint">Re</span>freshPoint
                </h1>

                <input
                    type="password"
                    name="password"
                    autoFocus
                    autoComplete="off"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                    onKeyDown={handleKeyDown}
                    onSubmit={handleSubmit}
                    className={`
                        w-full text-3xl border-4  py-4 px-8  rounded-xl  my-4 text-center border-neutral-100 bg-neutral-100
                        hover:bg-neutral-50 hover:border-freshpoint
                        focus:bg-white focus:border-freshpoint focus:ring-freshpoint focus:ring-opacity-30 focus:ring-4 focus:outline-none
                    `}
                />
            </div>
        </main>
    );
}
