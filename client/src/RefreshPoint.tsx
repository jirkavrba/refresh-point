import {FC} from "react";
import {PasswordAuthenticationContext} from "./context/PasswordAuthenticationContext.ts";
import {useLocalStorage} from "@uidotdev/usehooks";
import {RefreshPointApiContext} from "./context/RefreshPointApiContext.ts";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {LoginScreen} from "./pages/LoginScreen.tsx";
import {WatchdogConfigurationScreen} from "./pages/WatchdogConfigurationScreen.tsx";

export type RefreshPointProps = {
    api: string;
};

export const RefreshPoint: FC<RefreshPointProps> = ({api}) => {
    const client = new QueryClient();
    const [password, setPassword] = useLocalStorage<string | null>("refreshpoint-password", null);

    return (
        <QueryClientProvider client={client}>
            <RefreshPointApiContext.Provider value={{api}}>
                <PasswordAuthenticationContext.Provider value={{password, setPassword}}>
                    {
                        password === null
                            ? <LoginScreen/>
                            : <WatchdogConfigurationScreen/>
                    }
                </PasswordAuthenticationContext.Provider>
            </RefreshPointApiContext.Provider>
        </QueryClientProvider>
    )
};
