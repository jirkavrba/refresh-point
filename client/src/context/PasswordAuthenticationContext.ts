import React, {useContext} from "react";

export type PasswordAuthenticationContextState = {
    password: string | null;
    setPassword: (password: string | null) => void;
};

export const PasswordAuthenticationContext = React.createContext<PasswordAuthenticationContextState | null>(null);

export const usePasswordAuthenticationContext = (): PasswordAuthenticationContextState => {
    const value = useContext(PasswordAuthenticationContext);

    if (value === null) {
        throw Error("The password authentication context has not been properly initialized!");
    }

    return value;
}
