import React, {useContext} from "react";

export type RefreshPointApiContextState = {
    api: string;
};

export const RefreshPointApiContext = React.createContext<RefreshPointApiContextState | null>(null);

export const useRefreshpointApiContext = (): RefreshPointApiContextState => {
    const value = useContext(RefreshPointApiContext);

    if (value === null) {
        throw Error("The RefreshPoint API context has not been properly initialized!");
    }

    return value;
}
