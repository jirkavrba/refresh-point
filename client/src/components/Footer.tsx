import {FC} from "react";
import {FaHeart} from "react-icons/fa6";

export const Footer: FC = () => {
    return (
        <footer className="flex flex-col xl:flex-row items-center justify-between text-neutral-400 bg-neutral-100 font-bold uppercase text-xs mx-4 p-4 rounded-t-2xl mt-8 gap-2">
            <div>&copy; 2024 Jiří Vrba</div>
            <div className="text-neutral-300 text-center">
                napsáno s <FaHeart className="inline-block text-red-200"/> ve Springu / Reactu,
                klidně mi pošlete
                <a
                    href="https://gitlab.com/jirkavrba/refresh-point"
                    target="_blank"
                    className="mx-1 underline decoration-wavy cursor-pointer transition hover:decoration-orange-600 hover:text-orange-600"
                >
                    merge request
                </a> :)
            </div>
        </footer>
    )
}
