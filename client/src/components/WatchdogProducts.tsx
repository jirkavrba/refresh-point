import {Watchdog} from "../api/Watchdog.ts";
import {Product} from "../api/Product.ts";
import {FC, useMemo, useState} from "react";
import {FaTrash, FaGlasses, FaCubesStacked, FaCoins} from "react-icons/fa6";
import {WatchdogProductSelection} from "./WatchdogProductSelection.tsx";

export type WatchdogProductsProps = {
    products: Array<Product>;
    watchdog: Watchdog;
    onUpdate: (updated: Watchdog) => void;
}

export const WatchdogProducts: FC<WatchdogProductsProps> = ({products, watchdog, onUpdate}) => {
    const [productSelectionOpen, setProductSelectionOpen] = useState<boolean>(false);

    const mappedWatchedProducts = useMemo(
        () => products.filter(product => watchdog.watchedProducts.includes(product.id)),
        [products, watchdog]
    );

    const availableProductsForSelection = useMemo(
        () => products.filter(product => !watchdog.watchedProducts.includes(product.id)),
        [products, watchdog]
    );

    const removeTrackedProduct = (id: number) => {
        onUpdate({...watchdog, watchedProducts: watchdog.watchedProducts.filter(product => product != id)})
    };

    const addTrackedProduct = (id: number) => {
        onUpdate({...watchdog, watchedProducts: [...watchdog.watchedProducts, id]})
        setProductSelectionOpen(false);
    };

    return (
        <div className="flex flex-col gap-4 items-stretch">
            <div className="max-h-[50vh] bg-neutral-100 flex flex-col gap-2 overflow-y-scroll rounded-xl snap-proximity p-4">
                {mappedWatchedProducts.map(product => (
                    <div key={product.id} className="flex flex-col xl:flex-row items-stretch justify-between bg-white border-2 border-neutral-200 rounded-xl">
                        <div className="flex flex-col gap-4 items-start justify-between p-4 max-w-64">
                            <img src={product.image} alt={product.name} className="h-8"/>
                            <span className="text-center xl:text-left text-sm font-bold text-neutral-500">{product.name}</span>
                            <div className="flex flex-row items-center justify-center gap-4">
                                <div className="flex flex-row items-center justify-center gap-2" title="Počet kusů v lednici">
                                    <FaCubesStacked className="text-neutral-300"/>
                                    <span className="text-neutral-700 font-bold">{product.quantity}</span>
                                </div>

                                <div className="flex flex-row items-center justify-center gap-2" title="Cena">
                                    <FaCoins className="text-neutral-300"/>
                                    {
                                        product.discountedPrice === null
                                            ? <span className="text-neutral-700 font-bold">{product.price} Kč</span>
                                            : (
                                                <>
                                                    <div className="relative text-neutral-300 mr-2">
                                                        <span>{product.price} Kč</span>
                                                        <div className="absolute w-full h-0.5 left-0 bottom-1 bg-neutral-400 origin-bottom-left -rotate-[15deg]"/>
                                                    </div>
                                                    <span className="text-red-600 font-bold">{product.discountedPrice} Kč</span>
                                                </>
                                            )
                                    }
                                </div>
                            </div>
                        </div>
                        <div
                            className="flex flex-col items-center justify-center bg-neutral-100 text-neutral-300 rounded-xl m-2 cursor-pointer transition hover:bg-red-200 hover:text-red-800"
                            onClick={() => removeTrackedProduct(product.id)}
                        >
                            <FaTrash className="w-4 h-4 m-4 lg:my-0 lg:mx-4"/>
                        </div>
                    </div>
                ))}
            </div>

            <div className={`
                    flex flex-col items-center justify-center gap-4 bg-neutral-50 border-2 border-dashed border-neutral-300 p-8 rounded-xl cursor-pointer transition shadow-sm 
                    hover:bg-freshpoint hover:border-freshpoint hover:border-solid hover:shadow-xl group
                `}
                 onClick={() => setProductSelectionOpen(true)}
            >
                <FaGlasses className="text-neutral-400 w-8 h-8 transition group-hover:text-white"/>
                <span className="text-neutral-500 uppercase font-bold text-sm transition group-hover:text-white">Přidat sledovaný produkt</span>
            </div>

            {productSelectionOpen && (
                <WatchdogProductSelection
                    availableProducts={availableProductsForSelection}
                    onSelect={(product) => addTrackedProduct(product.id)}
                    onClose={() => setProductSelectionOpen(false)}
                />
            )}
        </div>
    );
}
