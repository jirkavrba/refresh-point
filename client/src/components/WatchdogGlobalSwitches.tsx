import {Watchdog} from "../api/Watchdog.ts";
import {FC} from "react";
import classnames from "classnames";
import {IconType} from "react-icons";
import {FaCertificate, FaMagnifyingGlassDollar, FaRecycle, FaBoxOpen} from "react-icons/fa6";

type GlobalSwitchProps = {
    name: string;
    icon: IconType;
    enabled: boolean;
    onChange: (value: boolean) => void;
}

const GlobalSwitch: FC<GlobalSwitchProps> = ({name, icon, enabled, onChange}) => {
    const Icon = icon;
    return (
        <div className={classnames(
            "flex flex-row xl:flex-col items-center justify-between xl:justify-center gap-4 p-8 rounded-xl cursor-pointer transition shadow-sm hover:shadow-xl border-2 select-none",
            enabled
                ? "bg-freshpoint text-white border-freshpoint border-solid"
                : "bg-neutral-50 border-dashed border-neutral-300 hover:bg-neutral-100"
        )} onClick={() => onChange(!enabled)}>
            <Icon className={classnames(
                "w-10 h-10 xl:w-8 xl:h-8",
                enabled ? "text-white" : "text-neutral-400"
            )}/>

            <span className={classnames("uppercase font-bold text-sm text-left", enabled ? "text-white" : "text-neutral-500")}>
                {name}
            </span>
        </div>
    );
}

export type WatchdogGlobalSwitchesProps = {
    watchdog: Watchdog;
    onUpdate: (updated: Watchdog) => void;
};

export const WatchdogGlobalSwitches: FC<WatchdogGlobalSwitchesProps> = ({watchdog, onUpdate}) => {
    return (
        <div className="flex flex-col gap-4 items-stretch">
            <GlobalSwitch
                name="Nově přidané produkty"
                icon={FaCertificate}
                enabled={watchdog.announceNewProducts}
                onChange={(value) => onUpdate({...watchdog, announceNewProducts: value})}
            />

            <GlobalSwitch
                name="Produkty ve slevě"
                icon={FaMagnifyingGlassDollar}
                enabled={watchdog.announceDiscountedProducts}
                onChange={(value) => onUpdate({...watchdog, announceDiscountedProducts: value})}
            />

            <GlobalSwitch
                name="Doplněné produkty"
                icon={FaRecycle}
                enabled={watchdog.announceRefilledProducts}
                onChange={(value) => onUpdate({...watchdog, announceRefilledProducts: value})}
            />

            <GlobalSwitch
                name="Vyprodané produkty"
                icon={FaBoxOpen}
                enabled={watchdog.announceSoldOutProducts}
                onChange={(value) => onUpdate({...watchdog, announceSoldOutProducts: value})}
            />
        </div>
    );
};
