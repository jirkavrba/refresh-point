import {FaRightFromBracket} from "react-icons/fa6";
import {usePasswordAuthenticationContext} from "../context/PasswordAuthenticationContext.ts";

export const Header = () => {
    const {setPassword} = usePasswordAuthenticationContext();
    const signOut = () => {
        setPassword(null);
    };

    return (
        <header className="flex flex-row items-center justify-between bg-neutral-900 mx-4 p-4 rounded-b-2xl mb-8">
            <div className="flex flex-row items-center gap-2 text-xl font-black text-white">
                <img src="/logo.png" alt="Refreshpoint" className="w-8 h-8"/>
                <p>
                    <span className="text-freshpoint">Re</span>freshPoint
                </p>
            </div>
            <button className="flex flex-row items-center gap-2 bg-white py-2 px-4 rounded-xl font-semibold" onClick={signOut}>
                <FaRightFromBracket/>
                Odhlásit se
            </button>
        </header>
    )
}
