import {Watchdog} from "../api/Watchdog.ts";
import {Product} from "../api/Product.ts";
import {FC} from "react";
import {FaCubesStacked, FaArrowRight, FaArrowDown, FaPlus, FaSlack} from "react-icons/fa6";
import {WatchdogGlobalSwitches} from "./WatchdogGlobalSwitches.tsx";
import {WatchdogProducts} from "./WatchdogProducts.tsx";
import {useUpdateWatchdog} from "../api/queries/useUpdateWatchdog.ts";

const DividerArrow = () => {
    return (
        <>
            <FaArrowDown className="w-8 h-8 text-neutral-300 xl:hidden"/>
            <FaArrowRight className="w-8 h-8 text-neutral-300 hidden xl:block"/>
        </>
    )
};

export type WatchdogConfigurationProps = {
    watchdog: Watchdog;
    products: Array<Product>;
};

export const WatchdogConfiguration: FC<WatchdogConfigurationProps> = ({watchdog, products}) => {
    const {mutate} = useUpdateWatchdog();
    const handleWatchdogUpdate = (updated: Watchdog) => {
        mutate(updated);
    }

    return (
        <div className="container flex mx-auto flex-col xl:flex-row gap-4 items-center justify-between flex-grow px-12 xl:px-24 mb-8">
            <div className="flex flex-col items-center gap-4 text-freshpoint">
                <FaCubesStacked className="w-16 h-16"/>
                <span className="text-2xl font-bold">
                  FreshPoint
                </span>
            </div>

            <DividerArrow/>

            <WatchdogGlobalSwitches
                watchdog={watchdog}
                onUpdate={handleWatchdogUpdate}
            />

            <FaPlus className="w-8 h-8 text-neutral-300"/>

            <WatchdogProducts
                products={products}
                watchdog={watchdog}
                onUpdate={handleWatchdogUpdate}
            />

            <DividerArrow/>

            <div className="flex flex-col items-center gap-4 text-cyan-500">
                <FaSlack className="w-16 h-16"/>
                <span className="text-2xl font-bold text-center">
                    Slack webhook
                </span>
            </div>
        </div>
    );
}
