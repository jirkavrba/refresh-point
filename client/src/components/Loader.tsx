import {FaArrowsRotate} from "react-icons/fa6";
export const Loader = () => {
    return (
        <div className="flex flex-row items-center justify-center w-screen h-screen bg-freshpoint">
            <FaArrowsRotate className="w-16 h-16 text-white animate-spin"/>
        </div>
    )
};
