import {Product} from "../api/Product.ts";
import {FC} from "react";
import {FaXmark} from "react-icons/fa6";
import classnames from "classnames";

export type WatchdogProductSelectionProps = {
    availableProducts: Array<Product>;
    onSelect: (product: Product) => void;
    onClose: () => void;
}

export const WatchdogProductSelection: FC<WatchdogProductSelectionProps> = ({availableProducts, onSelect, onClose}) => {
    return (
        <div className="fixed left-0 top-0 bg-black bg-opacity-10 flex flex-col gap-8 items-center justify-between backdrop-blur-sm w-screen h-screen p-8 lg:p-16">
            <div className="bg-white p-8 rounded-xl self-stretch">
                <div className="flex flex-row items-start justify-between">
                    <span className="text-2xl font-black">
                      Vybrat produkt ke sledování
                    </span>
                    <FaXmark className="w-8 h-8 cursor-pointer transition hover:rotate-90" onClick={onClose}/>
                </div>
                <div className="mt-8 grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-4 gap-4 max-h-[60vh] overflow-y-scroll p-8 border rounded-xl shadow-inner">
                    {availableProducts.map(product => (
                        <div key={product.id}
                             className={classnames(
                                 "border-2 rounded-xl flex flex-col gap-4 items-center justify-between p-8 transition-all shadow-sm hover:shadow-lg cursor-pointer",
                                 product.quantity === 0 && "grayscale-[80%] hover:grayscale-0"
                             )}
                             onClick={() => onSelect(product)}
                        >
                            <img src={product.image} alt={product.name} className="h-16 rounded-lg"/>
                            <span className={classnames(
                                "text-center text-xl font-bold",
                                product.quantity === 0 ? "text-neutral-500" : "text-freshpoint"
                            )}>
                                {product.name}
                            </span>
                            <span className="text-center text-xs font-bold text-neutral-400">
                                {/* TODO: Refactor this shit */}
                                Aktuálně {product.quantity > 1 && product.quantity < 5 ? "jsou" : "je"} v lednici {product.quantity} {product.quantity === 1 ? "kus" : (product.quantity >= 5 ? "kusů" : "kusy")}
                                {
                                    product.discountedPrice === null
                                        ? <span> za cenu {product.price} Kč</span>
                                        : (
                                            <>
                                                <br/>
                                                za <span className="text-red-600 font-bold">{product.discountedPrice} Kč</span> místo původních {product.price} Kč
                                            </>
                                        )
                                }
                            </span>
                        </div>
                    ))}
                </div>
            </div>
            <button className="bg-freshpoint text-white px-8 py-4 rounded-xl font-bold" onClick={onClose}>
                Zavřít nabídku produktů
            </button>
        </div>
    );
};
