export type AuthorizationHeader = {
    "Authorization": string;
}

export const authorization = (password: string | null): AuthorizationHeader => {
    const credentials = btoa(`user:${password}`);

    return {
        "Authorization": `Basic ${credentials}`
    }
}
