export type Product = {
    id: number;
    name: string;
    image: string;
    quantity: number;
    price: number;
    discountedPrice: number | null;
};
