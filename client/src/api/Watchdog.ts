export type Watchdog = {
    announceNewProducts: boolean;
    announceDiscountedProducts: boolean;
    announceRefilledProducts: boolean;
    announceSoldOutProducts: boolean;
    watchedProducts: Array<number>;
};
