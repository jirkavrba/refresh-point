import {useMutation, useQueryClient} from "@tanstack/react-query";
import {useRefreshpointApiContext} from "../../context/RefreshPointApiContext.ts";
import {usePasswordAuthenticationContext} from "../../context/PasswordAuthenticationContext.ts";
import {Watchdog} from "../Watchdog.ts";
import {authorization} from "../helpers.ts";

export const useUpdateWatchdog = () => {
    const {api} = useRefreshpointApiContext();
    const {password} = usePasswordAuthenticationContext();

    const client = useQueryClient();

    return useMutation<Watchdog, Error, Watchdog>({
        mutationFn: async (watchdog: Watchdog): Promise<Watchdog> => {
            // Optimistic update :)
            await client.setQueryData(["watchdog"], watchdog);
            const response = await fetch(`${api}/api/watchdog/update`, {
                method: "PUT",
                body: JSON.stringify(watchdog),
                headers: {
                    ...authorization(password),
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
            });

            return response.json();
        },
        onSuccess: async (response) => {
            await Promise.all([
                client.setQueryData(["watchdog"], response),
            ]);
        }
    })
}
