import {useRefreshpointApiContext} from "../../context/RefreshPointApiContext.ts";
import {usePasswordAuthenticationContext} from "../../context/PasswordAuthenticationContext.ts";
import {useQuery} from "@tanstack/react-query";
import {authorization} from "../helpers.ts";
import {Product} from "../Product.ts";

export const useProducts = () => {
    const {api} = useRefreshpointApiContext();
    const {password} = usePasswordAuthenticationContext();

    return useQuery({
        queryKey: ["products"],
        queryFn: async (): Promise<Array<Product>> => {
            const response = await fetch(`${api}/api/products`, {
                method: "GET",
                headers: authorization(password),
            });

            const {products} = await response.json();
            return products;
        }
    });
};
