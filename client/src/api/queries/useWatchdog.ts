import {useRefreshpointApiContext} from "../../context/RefreshPointApiContext.ts";
import {usePasswordAuthenticationContext} from "../../context/PasswordAuthenticationContext.ts";
import {useQuery} from "@tanstack/react-query";
import {authorization} from "../helpers.ts";
import {Watchdog} from "../Watchdog.ts";

export const useWatchdog = () => {
    const {api} = useRefreshpointApiContext();
    const {password} = usePasswordAuthenticationContext();

    return useQuery({
        queryKey: ["watchdog"],
        queryFn: async (): Promise<Watchdog> => {
            const response = await fetch(`${api}/api/watchdog`, {
                method: "GET",
                headers: authorization(password),
            });

            return response.json();
        }
    });
};
