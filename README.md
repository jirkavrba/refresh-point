![RefreshPoint logo](https://gitlab.com/uploads/-/system/project/avatar/54171945/refresh-point.png?width=96)

# RefreshPoint

Monitoring FreshPoint fridges so you don't have to.
