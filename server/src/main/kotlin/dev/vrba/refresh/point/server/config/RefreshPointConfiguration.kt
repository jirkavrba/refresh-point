package dev.vrba.refresh.point.server.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.Name

@ConfigurationProperties(prefix = "refresh-point")
data class RefreshPointConfiguration(
    @Name("fresh-point-url") val freshPointUrl: String,
    @Name("slack-webhook-url") val slackWebhookUrl: String,
    @Name("master-password") val password: String,
)
