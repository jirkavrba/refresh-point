package dev.vrba.refresh.point.server.response

import dev.vrba.refresh.point.server.dto.ProductDto

data class ProductResponse(
    val products: List<ProductDto>
)
