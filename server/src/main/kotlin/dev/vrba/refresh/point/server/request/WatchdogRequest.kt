package dev.vrba.refresh.point.server.request

data class WatchdogRequest(
    val announceNewProducts: Boolean,
    val announceDiscountedProducts: Boolean,
    val announceRefilledProducts: Boolean,
    val announceSoldOutProducts: Boolean,
    val watchedProducts: List<Int>,
)
