package dev.vrba.refresh.point.server.response

import dev.vrba.refresh.point.server.domain.Watchdog

data class WatchdogResponse(
    val announceNewProducts: Boolean,
    val announceDiscountedProducts: Boolean,
    val announceRefilledProducts: Boolean,
    val announceSoldOutProducts: Boolean,
    val watchedProducts: List<Int>,
)

fun Watchdog.toResponse() = WatchdogResponse(
    announceNewProducts = announceNewProducts,
    announceDiscountedProducts = announceDiscountedProducts,
    announceRefilledProducts = announceRefilledProducts,
    announceSoldOutProducts = announceSoldOutProducts,
    watchedProducts = watchedProducts
)
