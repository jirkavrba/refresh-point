package dev.vrba.refresh.point.server.service

import dev.vrba.refresh.point.server.config.RefreshPointConfiguration
import dev.vrba.refresh.point.server.domain.ChangedProductMessage
import dev.vrba.refresh.point.server.domain.DiscountedProductMessage
import dev.vrba.refresh.point.server.domain.NewProductMessage
import dev.vrba.refresh.point.server.domain.ProductsDiffMessage
import dev.vrba.refresh.point.server.domain.RefilledProductMessage
import dev.vrba.refresh.point.server.domain.SoldOutProductMessage
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitExchange

@Service
class SlackNotificationsService(
    private val configuration: RefreshPointConfiguration
) {

    private val client: WebClient = WebClient.create()

    fun sendSlackNotification(messages: List<ProductsDiffMessage>) {
        if (messages.isEmpty()) {
            return
        }

        val message = messages.joinToString("\n\n") {
           when (it)  {
               is NewProductMessage -> it.buildMessage()
               is DiscountedProductMessage -> it.buildMessage()
               is RefilledProductMessage -> it.buildMessage()
               is SoldOutProductMessage -> it.buildMessage()
               is ChangedProductMessage -> it.buildMessage()
           }
        }

        runBlocking {
            client.post()
                .uri(configuration.slackWebhookUrl)
                .bodyValue(mapOf("text" to message))
                .awaitExchange {  }
        }
    }

    private fun NewProductMessage.buildMessage() =
        "✨ Byl přidán nový produkt: *${product.name}* - \uD83E\uDE99 ${product.price.formatAsPrice()}, \uD83D\uDCE6 ${product.quantity} kusů v lednici"

    private fun DiscountedProductMessage.buildMessage() =
        "\uD83D\uDCB8 Byl zlevněn produkt *${product.name}* z původní ceny *${product.price.formatAsPrice()}* na *${product.discountedPrice?.formatAsPrice()}*. Aktuálně je v lednici ${product.quantity} kusů."

    private fun RefilledProductMessage.buildMessage() =
        "♻\uFE0F Produkt *${product.name}* byl doplněn. Nový počet produktů v lednici je *${product.quantity}*"

    private fun SoldOutProductMessage.buildMessage() =
        "⚠\uFE0F Produkt *${product.name}* byl vyprodán."

    private fun ChangedProductMessage.buildMessage(): String {
        val changes = listOfNotNull(
            if (previousProduct.name != product.name) "změněn název z *${previousProduct.name}* na *${product.name}*" else null,
            if (previousProduct.quantity != product.quantity) "změněn počet kusů v lednici z *${previousProduct.quantity}* na *${product.quantity}*" else null,
            if (previousProduct.price != product.price) "změněna cena z *${previousProduct.price.formatAsPrice()}* na *${product.price.formatAsPrice()}*" else null,
            when {
                previousProduct.discountedPrice == null && product.discountedPrice != null -> "produkt byl zlevněn z *${product.price.formatAsPrice()}* na *${product.discountedPrice.formatAsPrice()}*"
                previousProduct.discountedPrice != null && product.discountedPrice == null -> "byla zrušena sleva a produkt opět stojí *${product.price.formatAsPrice()}*"
                previousProduct.discountedPrice != product.discountedPrice -> "byla změněna sleva z *${previousProduct.discountedPrice.formatAsPrice()}* na *${product.discountedPrice.formatAsPrice()}*"
                else -> null
            }
        )

        return "\uD83D\uDD0D Došlo ke změnám u sledovaného produktu *${product.name}*:\n" + changes.joinToString("\n") { "• $it" }
    }

    private fun Double?.formatAsPrice() =
        this?.let { String.format("%.2f", this).replace(".", ",") + " Kč" }
            ?: "-"
}
