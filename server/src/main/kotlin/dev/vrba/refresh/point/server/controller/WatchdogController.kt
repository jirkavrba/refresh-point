package dev.vrba.refresh.point.server.controller

import dev.vrba.refresh.point.server.request.WatchdogRequest
import dev.vrba.refresh.point.server.response.WatchdogResponse
import dev.vrba.refresh.point.server.response.toResponse
import dev.vrba.refresh.point.server.service.WatchdogService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/watchdog")
class WatchdogController(
    private val service: WatchdogService
) {

    @GetMapping
    suspend fun get(): ResponseEntity<WatchdogResponse> {
        val watchdog = service.getDefaultWatchdog()
        val response = watchdog.toResponse()

        return ResponseEntity.ok(response)
    }

    @PutMapping("/update")
    suspend fun update(@RequestBody request: WatchdogRequest): ResponseEntity<WatchdogResponse> {
        val watchdog = service.updateDefaultWatchdog(
            announceNewProducts = request.announceNewProducts,
            announceDiscountedProducts = request.announceDiscountedProducts,
            announceRefilledProducts = request.announceRefilledProducts,
            announceSoldOutProducts = request.announceSoldOutProducts,
            watchedProducts = request.watchedProducts
        )

        val response = watchdog.toResponse()

        return ResponseEntity.ok(response)
    }
}
