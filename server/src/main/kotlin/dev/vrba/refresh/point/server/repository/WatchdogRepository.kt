package dev.vrba.refresh.point.server.repository

import dev.vrba.refresh.point.server.domain.Watchdog
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WatchdogRepository : CrudRepository<Watchdog, String>
