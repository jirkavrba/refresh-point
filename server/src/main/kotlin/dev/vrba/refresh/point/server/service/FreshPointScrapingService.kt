package dev.vrba.refresh.point.server.service

import dev.vrba.refresh.point.server.config.RefreshPointConfiguration
import dev.vrba.refresh.point.server.domain.Product
import it.skrape.core.htmlDocument
import it.skrape.fetcher.HttpFetcher
import it.skrape.fetcher.response
import it.skrape.fetcher.skrape
import org.springframework.stereotype.Service

@Service
class FreshPointScrapingService(private val configuration: RefreshPointConfiguration) {

    fun scrapeProducts(): List<Product> {
        return skrape(HttpFetcher) {
            request {
                url = configuration.freshPointUrl
                timeout = 15000
            }

            response {
                htmlDocument {
                    findAll(".product").map {
                        val id = it.attribute("data-id").toInt()
                        val name = it.attribute("data-name")
                        val image = it.attribute("data-photourl")
                        val quantity = if (it.hasClass("sold-out")) 0 else {
                            it.findFirst(".col-6.pr-md-3 > .px-2.font-italic.font-weight-bold") {
                                when (val content = text.trim()) {
                                    "Poslední kus!" -> 1
                                    else -> content.split(" ").first().toInt()
                                }
                            }
                        }

                        val container = it.findFirst(".col-6.pl-md-3")
                        val price = container.children.first().text.toDouble()
                        val discountedPrice = container.children.getOrNull(1)?.text?.toDouble()

                        Product(
                            id = id,
                            name = name,
                            image = image,
                            quantity = quantity,
                            price = price,
                            discountedPrice = discountedPrice
                        )
                    }
                }
            }
        }
    }
}
