package dev.vrba.refresh.point.server.domain

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

@RedisHash("watchdog")
data class Watchdog(
    @Id
    val id: String = DEFAULT_ID,
    val announceNewProducts: Boolean = true,
    val announceDiscountedProducts: Boolean = true,
    val announceRefilledProducts: Boolean = false,
    val announceSoldOutProducts: Boolean = false,
    val watchedProducts: List<Int> = emptyList(),
) {
    companion object {
        const val DEFAULT_ID: String = "default"
    }
}
