package dev.vrba.refresh.point.server.security

import dev.vrba.refresh.point.server.config.RefreshPointConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.config.web.server.invoke
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.util.matcher.PathPatternParserServerWebExchangeMatcher as PathMatcher

@Configuration
@EnableWebFluxSecurity
class SecurityConfiguration {

    @Bean
    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

    @Bean
    fun securityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        return http {
            httpBasic {}
            formLogin { disable() }
            cors { disable() }
            csrf { disable() }
            authorizeExchange {
                authorize(PathMatcher("/api/**", HttpMethod.GET), hasRole("USER"))
                authorize(PathMatcher("/api/**", HttpMethod.POST), hasRole("USER"))
                authorize(anyExchange, permitAll)
            }
        }
    }

    @Bean
    fun userDetailService(passwordEncoder: PasswordEncoder, configuration: RefreshPointConfiguration): ReactiveUserDetailsService {
        val user = User
            .withUsername("user")
            .password(passwordEncoder.encode(configuration.password))
            .roles("USER")
            .build()

        return MapReactiveUserDetailsService(user)
    }
}

