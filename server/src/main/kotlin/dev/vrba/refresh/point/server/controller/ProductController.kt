package dev.vrba.refresh.point.server.controller

import dev.vrba.refresh.point.server.dto.toDto
import dev.vrba.refresh.point.server.response.ProductResponse
import dev.vrba.refresh.point.server.service.ProductService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/products")
class ProductController(private val service: ProductService) {

    @GetMapping
    suspend fun list(): ResponseEntity<ProductResponse> {
        return withContext(Dispatchers.IO) {
            val products = service.listProducts()
            val response = ProductResponse(products = products.map { it.toDto() })

            ResponseEntity.ok(response)
        }
    }
}
