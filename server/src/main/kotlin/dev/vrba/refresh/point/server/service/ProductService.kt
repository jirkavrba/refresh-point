package dev.vrba.refresh.point.server.service

import dev.vrba.refresh.point.server.domain.Product
import dev.vrba.refresh.point.server.repository.ProductRepository
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

@Service
class ProductService(
    private val scraper: FreshPointScrapingService,
    private val repository: ProductRepository,
    private val watchdogService: WatchdogService,
    private val diffService: ProductDiffService,
    private val notificationsService: SlackNotificationsService
) {

    private val logger = LoggerFactory.getLogger(this::class.qualifiedName)

    fun listProducts(): List<Product> =
        repository.findAll().toList()

    @Scheduled(initialDelay = 0, fixedDelay = 5, timeUnit = TimeUnit.MINUTES)
    fun updateProducts() {
        val previous = listProducts()
        val current = scraper.scrapeProducts()

        logger.info("Downloaded ${current.size} products")

        val watchdog = watchdogService.getDefaultWatchdog()
        val diff = diffService.createDiff(previous, current)
        val messages = diffService.createMessagesFromDiff(diff, watchdog)

        repository.saveAll(current)
        notificationsService.sendSlackNotification(messages)
    }

}
