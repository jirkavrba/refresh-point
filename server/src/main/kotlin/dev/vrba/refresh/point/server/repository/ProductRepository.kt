package dev.vrba.refresh.point.server.repository

import dev.vrba.refresh.point.server.domain.Product
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : CrudRepository<Product, Int>
