package dev.vrba.refresh.point.server.dto

import dev.vrba.refresh.point.server.domain.Product

data class ProductDto(
    val id: Int,
    val name: String,
    val image: String,
    val quantity: Int,
    val price: Double,
    val discountedPrice: Double? = null
)

fun Product.toDto() = ProductDto(
    id = id,
    name = name,
    image = image,
    quantity = quantity,
    price = price,
    discountedPrice = discountedPrice
)
