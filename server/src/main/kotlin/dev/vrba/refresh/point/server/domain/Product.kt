package dev.vrba.refresh.point.server.domain

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

@RedisHash("product")
data class Product(
    @Id
    val id: Int,
    val name: String,
    val image: String,
    val quantity: Int,
    val price: Double,
    val discountedPrice: Double? = null,
) {
    val effectivePrice: Double
        get() = discountedPrice ?: price
}
