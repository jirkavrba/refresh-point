package dev.vrba.refresh.point.server.service

import dev.vrba.refresh.point.server.domain.ChangedProductMessage
import dev.vrba.refresh.point.server.domain.DiscountedProductMessage
import dev.vrba.refresh.point.server.domain.NewProductMessage
import dev.vrba.refresh.point.server.domain.Product
import dev.vrba.refresh.point.server.domain.ProductsDiff
import dev.vrba.refresh.point.server.domain.ProductsDiffMessage
import dev.vrba.refresh.point.server.domain.RefilledProductMessage
import dev.vrba.refresh.point.server.domain.SoldOutProductMessage
import dev.vrba.refresh.point.server.domain.Watchdog
import org.springframework.stereotype.Service

@Service
class ProductDiffService {

    fun createDiff(previous: List<Product>, current: List<Product>): ProductsDiff {
        val previousProductIds = previous.map { it.id }
        val productsZippedWithPrevious = previous.mapNotNull { product -> current.firstOrNull { it.id == product.id }?.let { product to it } }
        
        val newProducts = current.filter { it.id !in previousProductIds }
        val discountedProducts = productsZippedWithPrevious.filter { (previous, current) -> previous.discountedPrice == null && current.discountedPrice != null }
        val refilledProducts = productsZippedWithPrevious.filter { (previous, current) -> previous.quantity < current.quantity }
        val changedProducts = productsZippedWithPrevious.filter { (previous, current) -> previous != current }
        val soldOutProducts = productsZippedWithPrevious.filter { (previous, current) -> previous.quantity != 0 && current.quantity == 0 }.map { it.second }

        return ProductsDiff(
            newProducts = newProducts,
            discountedProducts = discountedProducts,
            refilledProducts = refilledProducts,
            changedProducts = changedProducts,
            soldOutProducts = soldOutProducts
        )
    }

    fun createMessagesFromDiff(diff: ProductsDiff, watchdog: Watchdog): List<ProductsDiffMessage> {
        val messages = listOf(
            if (watchdog.announceNewProducts) diff.newProducts.map { NewProductMessage(it) } else emptyList(),
            if (watchdog.announceDiscountedProducts) diff.discountedProducts.map {
                DiscountedProductMessage(
                    it.second, it.first.effectivePrice, it.second.effectivePrice
                )
            } else emptyList(),
            if (watchdog.announceRefilledProducts) diff.refilledProducts.map {
                RefilledProductMessage(
                    it.second, it.first.quantity, it.second.quantity
                )
            } else emptyList(),
            if (watchdog.announceSoldOutProducts) diff.soldOutProducts.map { SoldOutProductMessage(it) } else emptyList(),
            diff.changedProducts
                .filter { it.second.id in watchdog.watchedProducts }
                .map { ChangedProductMessage(it.second, it.first) }
        )

        return messages.flatten()
    }

}
