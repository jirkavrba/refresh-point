package dev.vrba.refresh.point.server.service

import dev.vrba.refresh.point.server.domain.Watchdog
import dev.vrba.refresh.point.server.repository.WatchdogRepository
import jakarta.annotation.PostConstruct
import org.springframework.stereotype.Service

@Service
class WatchdogService(private val repository: WatchdogRepository) {

    @PostConstruct
    fun createDefaultWatchdog() {
        if (!repository.existsById(Watchdog.DEFAULT_ID)) {
            repository.save(Watchdog())
        }
    }

    fun getDefaultWatchdog(): Watchdog {
        return repository.findById(Watchdog.DEFAULT_ID).orElseThrow()
    }

    fun updateDefaultWatchdog(
        announceNewProducts: Boolean,
        announceDiscountedProducts: Boolean,
        announceRefilledProducts: Boolean,
        announceSoldOutProducts: Boolean,
        watchedProducts: List<Int>,
    ): Watchdog {
        val updated = getDefaultWatchdog().copy(
            announceNewProducts = announceNewProducts,
            announceDiscountedProducts = announceDiscountedProducts,
            announceRefilledProducts = announceRefilledProducts,
            announceSoldOutProducts = announceSoldOutProducts,
            watchedProducts = watchedProducts
        )

        return repository.save(updated)
    }
}
