package dev.vrba.refresh.point.server.domain

sealed interface ProductsDiffMessage {
    val product: Product
}

data class NewProductMessage(override val product: Product) : ProductsDiffMessage

data class SoldOutProductMessage(override val product: Product) : ProductsDiffMessage

data class DiscountedProductMessage(
    override val product: Product,
    val originalPrice: Double,
    val discountedPrice: Double
) : ProductsDiffMessage

data class RefilledProductMessage(
    override val product: Product,
    val previousQuantity: Int,
    val currentQuantity: Int
) : ProductsDiffMessage

data class ChangedProductMessage(
    override val product: Product,
    val previousProduct: Product,
) : ProductsDiffMessage
