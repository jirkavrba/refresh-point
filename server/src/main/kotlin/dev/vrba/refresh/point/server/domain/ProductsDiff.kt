package dev.vrba.refresh.point.server.domain

data class ProductsDiff(
    val newProducts: List<Product>,
    val discountedProducts: List<Pair<Product, Product>>,
    val refilledProducts: List<Pair<Product, Product>>,
    val changedProducts: List<Pair<Product, Product>>,
    val soldOutProducts: List<Product>,
)
